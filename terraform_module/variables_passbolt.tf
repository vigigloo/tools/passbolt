variable "passbolt_host" {
  type = string
}

variable "passbolt_database_host" {
  type = string
}
variable "passbolt_database_port" {
  type    = number
  default = 5432
}
variable "passbolt_database_username" {
  type = string
}
variable "passbolt_database_password" {
  type      = string
  sensitive = true
}
variable "passbolt_database_name" {
  type = string
}

variable "passbolt_gpg_private_key" {
  type        = string
  sensitive   = true
  description = "A GPG private key in PEM format"
}
variable "passbolt_gpg_public_key" {
  type        = string
  description = "A GPG public key in PEM format"
}
variable "passbolt_gpg_fingerprint" {
  type        = string
  description = "A GPG fingerprint"
}

variable "passbolt_jwt_private_key" {
  type        = string
  sensitive   = true
  description = "An RSA private key in PEM format"
}
variable "passbolt_jwt_public_key" {
  type        = string
  description = "An RSA public key in PEM format"
}

variable "passbolt_smtp_host" {
  type = string
}
variable "passbolt_smtp_port" {
  type = string
}
variable "passbolt_smtp_username" {
  type = string
}
variable "passbolt_smtp_password" {
  type      = string
  sensitive = true
}
variable "passbolt_smtp_tls" {
  type    = bool
  default = false
}
variable "passbolt_smtp_from_email" {
  type = string
}
variable "passbolt_smtp_from_name" {
  type    = string
  default = "Passbolt"
}
