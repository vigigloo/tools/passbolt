resource "helm_release" "passbolt" {
  chart           = "passbolt"
  repository      = "https://gitlab.com/api/v4/projects/40311187/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history
  timeout         = var.helm_timeout

  values = concat([
    file("${path.module}/passbolt.yaml"),
  ], var.values)

  set {
    name  = "image.repository"
    value = var.image_repository
  }
  set {
    name  = "image.tag"
    value = var.image_tag
  }

  dynamic "set" {
    for_each = compact([var.limits_cpu])
    content {
      name  = "resources.limits.cpu"
      value = set.value
    }
  }
  dynamic "set" {
    for_each = compact([var.limits_memory])
    content {
      name  = "resources.limits.memory"
      value = set.value
    }
  }
  dynamic "set" {
    for_each = compact([var.requests_cpu])
    content {
      name  = "resources.requests.cpu"
      value = set.value
    }
  }
  dynamic "set" {
    for_each = compact([var.requests_memory])
    content {
      name  = "resources.requests.memory"
      value = set.value
    }
  }

  set {
    name  = "ingress.host"
    value = var.passbolt_host
  }

  set {
    name  = "passbolt.database.host"
    value = var.passbolt_database_host
  }
  set {
    name  = "passbolt.database.port"
    value = var.passbolt_database_port
  }
  set {
    name  = "passbolt.database.username"
    value = var.passbolt_database_username
  }
  set {
    name  = "passbolt.database.password"
    value = var.passbolt_database_password
  }
  set {
    name  = "passbolt.database.name"
    value = var.passbolt_database_name
  }

  set {
    name  = "passbolt.gpg.privateKey"
    value = base64encode(var.passbolt_gpg_private_key)
  }
  set {
    name  = "passbolt.gpg.publicKey"
    value = base64encode(var.passbolt_gpg_public_key)
  }
  set {
    name  = "passbolt.gpg.fingerprint"
    value = upper(var.passbolt_gpg_fingerprint)
  }

  set {
    name  = "passbolt.jwt.privateKey"
    value = base64encode(var.passbolt_jwt_private_key)
  }
  set {
    name  = "passbolt.jwt.publicKey"
    value = base64encode(var.passbolt_jwt_public_key)
  }

  set {
    name  = "passbolt.smtp.host"
    value = var.passbolt_smtp_host
  }
  set {
    name  = "passbolt.smtp.port"
    value = var.passbolt_smtp_port
  }
  set {
    name  = "passbolt.smtp.username"
    value = var.passbolt_smtp_username
  }
  set {
    name  = "passbolt.smtp.password"
    value = var.passbolt_smtp_password
  }
  set {
    name  = "passbolt.smtp.tls"
    value = var.passbolt_smtp_tls
  }
  set {
    name  = "passbolt.smtp.from"
    value = var.passbolt_smtp_from_email
  }
  set {
    name  = "passbolt.smtp.fromName"
    value = var.passbolt_smtp_from_name
  }
}
